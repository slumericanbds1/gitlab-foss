# frozen_string_literal: true

require_relative '../../lib/gitlab/i18n'

Gitlab::I18n.setup(domain: 'gitlab', default_locale: :en)
